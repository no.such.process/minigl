#include <stdio.h>
#include <string.h>
#include <math.h>
#include <allegro.h>

/* From math.h with __USE_XOPEN: */
#define M_PI           3.14159265358979323846  /* pi */


struct coord {
  double x, y, z, w;
};

struct camera {
  struct coord center;
  struct coord coordsys[3];
  /* opening angles, in degree */
  double alpha_x;
  double alpha_y;
  /* double focal = 1; /\* (fixed) focal length *\/ */
};

struct vertices {
  struct coord a, b, c;
};

struct object {
  struct coord center;
  /* Array of triangles, each triangle is 3 vertices/points */
  struct vertices* polygons;
  int nb_polygons;
};

typedef double matrix[4][4];


extern void object_draw(struct camera, struct object);
extern void matrix_copy(matrix *M1, matrix *M2);



void matrix_display(matrix *M) {
  int i,j;
  printf("----------\n");
  for (i = 0; i < 4; i++) {
    for (j = 0; j < 4; j++)
      printf("%f ", (*M)[i][j]);
    printf("\n");
  }
  printf("----------\n");
}



void translation(matrix *M, double tx, double ty, double tz) {
  (*M)[0][0]= 1 ;(*M)[0][1]= 0 ;(*M)[0][2]= 0 ;(*M)[0][3]= tx ;
  (*M)[1][0]= 0 ;(*M)[1][1]= 1 ;(*M)[1][2]= 0 ;(*M)[1][3]= ty ;
  (*M)[2][0]= 0 ;(*M)[2][1]= 0 ;(*M)[2][2]= 1 ;(*M)[2][3]= tz ;
  (*M)[3][0]= 0 ;(*M)[3][1]= 0 ;(*M)[3][2]= 0 ;(*M)[3][3]=  1 ;
}

void rotationX(matrix *M, double a) {
  (*M)[0][0]= 1 ;(*M)[0][1]=      0 ;(*M)[0][2]=       0 ;(*M)[0][3]= 0 ;
  (*M)[1][0]= 0 ;(*M)[1][1]= cos(a) ;(*M)[1][2]= -sin(a) ;(*M)[1][3]= 0 ;
  (*M)[2][0]= 0 ;(*M)[2][1]= sin(a) ;(*M)[2][2]=  cos(a) ;(*M)[2][3]= 0 ;
  (*M)[3][0]= 0 ;(*M)[3][1]=      0 ;(*M)[3][2]=       0 ;(*M)[3][3]= 1 ;
}
void rotationY(matrix *M, double a) {
  (*M)[0][0]=  cos(a) ;(*M)[0][1]= 0 ;(*M)[0][2]= sin(a) ;(*M)[0][3]= 0 ;
  (*M)[1][0]=       0 ;(*M)[1][1]= 1 ;(*M)[1][2]=      0 ;(*M)[1][3]= 0 ;
  (*M)[2][0]= -sin(a) ;(*M)[2][1]= 0 ;(*M)[2][2]= cos(a) ;(*M)[2][3]= 0 ;
  (*M)[3][0]=       0 ;(*M)[3][1]= 0 ;(*M)[3][2]=      0 ;(*M)[3][3]= 1 ;
}
void rotationZ(matrix *M, double a) {
  (*M)[0][0]= cos(a) ;(*M)[0][1]= -sin(a) ;(*M)[0][2]= 0 ;(*M)[0][3]= 0 ;
  (*M)[1][0]= sin(a) ;(*M)[1][1]=  cos(a) ;(*M)[1][2]= 0 ;(*M)[1][3]= 0 ;
  (*M)[2][0]=      0 ;(*M)[2][1]=       0 ;(*M)[2][2]= 1 ;(*M)[2][3]= 0 ;
  (*M)[3][0]=      0 ;(*M)[3][1]=       0 ;(*M)[3][2]= 0 ;(*M)[3][3]= 1 ;
}

/* Multiply M1 by M2, modifies M1 */
void matrix_multiply(matrix *M1, matrix *M2) {
  matrix copy;
  int i,j;
  matrix_copy(M1, &copy);

  for (i = 0; i < 4; i++)
    for (j = 0; j < 4; j++) {
      int k;
      (*M1)[i][j] = 0;
      for (k = 0; k < 4; k++)
	(*M1)[i][j] += copy[i][k] * (*M2)[k][j];
    }
}

void matrix_copy(matrix *from, matrix *to) {
/*   int i, j; */
/*   for (i = 0; i < 4; i++) */
/*     for (j = 0; j < 4; j++) */
/*       (*to)[i][j] = (*from)[i][j]; */

  memcpy(to, from, sizeof(matrix));
}

void matrix_multiply_vector(matrix *M1, struct coord *c, struct coord *result) {
  result->x = (*M1)[0][0] * c->x + (*M1)[0][1] * c->y + (*M1)[0][2] * c->z + (*M1)[0][3] * c->w;
  result->y = (*M1)[1][0] * c->x + (*M1)[1][1] * c->y + (*M1)[1][2] * c->z + (*M1)[1][3] * c->w;
  result->z = (*M1)[2][0] * c->x + (*M1)[2][1] * c->y + (*M1)[2][2] * c->z + (*M1)[2][3] * c->w;
  result->w = (*M1)[3][0] * c->x + (*M1)[3][1] * c->y + (*M1)[3][2] * c->z + (*M1)[3][3] * c->w;
}

int main(void) {
  struct object obj;
  struct coord obj_center = {0,0,0,1};

  struct camera cam;

  allegro_init();
  install_keyboard();
  if (set_gfx_mode(GFX_AUTODETECT_WINDOWED, 800, 600, 0, 0) != 0)
    perror("set_gfx_mode");

  /* Flat square
     b--a'
     |\ |
     | \|
     a--c
  */ 
  obj.center = obj_center;
  {
    /* Square */
    /*
    struct coord a = { -1, -1, 5,1};
    struct coord b = { -1,  1, 5,1};
    struct coord c = {  1, -1, 5,1};    
    struct coord a2 = {  1,  1, 5,1};

    obj.nb_polygons = 2;
    obj.polygons = malloc(obj.nb_polygons * sizeof(struct vertices));

    obj.polygons[0].a = a;
    obj.polygons[0].b = b;
    obj.polygons[0].c = c;

    obj.polygons[1].a = a2;
    obj.polygons[1].b = b;
    obj.polygons[1].c = c;
    */


    /* Cube */
    
    struct coord a = { -1,  1, 5,1};
    struct coord b = {  1,  1, 5,1};
    struct coord c = {  1, -1, 5,1};    
    struct coord d = { -1, -1, 5,1};
    struct coord e = { -1,  1, 3,1};
    struct coord f = {  1,  1, 3,1};
    struct coord g = {  1, -1, 3,1};    
    struct coord h = { -1, -1, 3,1};

    obj.nb_polygons = 12;
    obj.polygons = malloc(obj.nb_polygons * sizeof(struct vertices));

    obj.polygons[0].a = a;
    obj.polygons[0].b = b;
    obj.polygons[0].c = c;
    obj.polygons[1].a = a;
    obj.polygons[1].b = d;
    obj.polygons[1].c = c;

    obj.polygons[2].a = e;
    obj.polygons[2].b = f;
    obj.polygons[2].c = g;
    obj.polygons[3].a = e;
    obj.polygons[3].b = h;
    obj.polygons[3].c = g;

    obj.polygons[4].a = e;
    obj.polygons[4].b = a;
    obj.polygons[4].c = d;
    obj.polygons[5].a = e;
    obj.polygons[5].b = h;
    obj.polygons[5].c = d;

    obj.polygons[6].a = f;
    obj.polygons[6].b = b;
    obj.polygons[6].c = c;
    obj.polygons[7].a = f;
    obj.polygons[7].b = g;
    obj.polygons[7].c = c;

    obj.polygons[8].a = e;
    obj.polygons[8].b = f;
    obj.polygons[8].c = b;
    obj.polygons[9].a = e;
    obj.polygons[9].b = a;
    obj.polygons[9].c = b;

    obj.polygons[10].a = h;
    obj.polygons[10].b = g;
    obj.polygons[10].c = c;
    obj.polygons[11].a = h;
    obj.polygons[11].b = d;
    obj.polygons[11].c = c;    
    
  }


  /* Camera: centered on the 1x1x1 object */
  {
    struct coord cam_center = {0,0,10,1};
    cam.center = cam_center;
  }
  {
    /* Angle of view / field of vision */
    double screen_ratio = 1.0 * screen->w / screen->h;
    /* X fov: arbitrarily set to 66, waiting for a better suggestion */
    cam.alpha_x = 66 * (M_PI / 180);
    /* Y fov: computed from alpha_x and the screen aspect ratio. It's
       not as simple as (alpha_x / screen_ratio); draw the camera from
       the top and from the left to understand the formula */
    cam.alpha_y = 2 * atan(tan(cam.alpha_x/2) / screen_ratio);
    printf("alpha_x = %f\nalpha_y = %f\n", cam.alpha_x * 180 / M_PI, cam.alpha_y * 180 / M_PI);
  }

  /* facing the object (-Xo, Yo, -Zo) */
  {
    struct coord x = {-1, 0, 0,1};
    struct coord y = { 0, 1, 0,1};
    struct coord z = { 0, 0,-1,1};
    cam.coordsys[0] = x;
    cam.coordsys[1] = y;
    cam.coordsys[2] = z;
  }

  clear_to_color(screen, 15);
  object_draw(cam,obj);

  readkey();
  return 0;
}

void object_draw(struct camera cam, struct object obj) {
  /*
    - express the object coordinates in the camera's system
    - project each polygon on the camera's 3D screen
    - project each point from the camera's 3D screen to the 2D physical screen
   */

  /* camera resolution */
  double rx = screen->w;
  double ry = screen->h;
  /* (camera) -> (physical screen) units scaling factor */
  double sx = (rx / (2 * tan (cam.alpha_x / 2)));
  double sy = (ry / (2 * tan (cam.alpha_y / 2)));
  int i;

  /* coordsys change from rep1 to rep2
     = (transformation from rep1 to rep2)^-1
     = transformation from rep2 to rep1
  */
  double rotx = acos(cam.coordsys[0].x); /* acos(Xo.Xc) */
  double roty = acos(cam.coordsys[1].y); /* acos(Yo.Yc) */
  double rotz = acos(cam.coordsys[2].z); /* acos(Zo.Zc) */
  matrix WtoC, tmp;
  rotationX(&WtoC, -rotx);
  rotationY(&tmp, -roty);
  matrix_multiply(&WtoC, &tmp);
  rotationZ(&tmp, -rotz);
  matrix_multiply(&WtoC, &tmp);

  translation(&tmp, -cam.center.x, -cam.center.y, -cam.center.z);
  matrix_multiply(&WtoC, &tmp);

  
  for (i = 0; i < obj.nb_polygons; i++) {
    struct coord ac, bc, cc;
    struct coord ac2, bc2, cc2;
    int a_screen_x, a_screen_y,
      b_screen_x, b_screen_y,
      c_screen_x, c_screen_y;
    
    /* Express the object coordinates in the camera's system */
    
    matrix_multiply_vector(&WtoC, &obj.polygons[i].a, &ac);
    matrix_multiply_vector(&WtoC, &obj.polygons[i].b, &bc);
    matrix_multiply_vector(&WtoC, &obj.polygons[i].c, &cc);
    
    /* Project each polygon on the camera's 3D screen */
    /* Using d (focal length) = -1, so that X and Y are flipped, which
       matches the computer screen coordinates system */

    ac2.x = ac.x / - ac.z;
    ac2.y = ac.y / - ac.z;
    ac2.z = 1;

    bc2.x = bc.x / - bc.z;
    bc2.y = bc.y / - bc.z;
    bc2.z = 1;

    cc2.x = cc.x / - cc.z;
    cc2.y = cc.y / - cc.z;
    cc2.z = 1;
    
    /* Project each point from the camera's 3D screen to the 2D
       physical screen: scale by sx&sy, then center */

    /* reversing X&Y coordinates */
/*     a_screen_x = screen->w - (sx * ac2.x + rx/2); */
/*     b_screen_x = screen->w - (sx * bc2.x + rx/2); */
/*     c_screen_x = screen->w - (sx * cc2.x + rx/2); */
/*     a_screen_y = screen->h - (sy * ac2.y + ry/2); */
/*     b_screen_y = screen->h - (sy * bc2.y + ry/2); */
/*     c_screen_y = screen->h - (sy * cc2.y + ry/2); */
    a_screen_x = sx * ac2.x + rx/2;
    b_screen_x = sx * bc2.x + rx/2;
    c_screen_x = sx * cc2.x + rx/2;
    a_screen_y = sy * ac2.y + ry/2;
    b_screen_y = sy * bc2.y + ry/2;
    c_screen_y = sy * cc2.y + ry/2;
    
    /* Actually draw on screen */
    line(screen, a_screen_x, a_screen_y, b_screen_x, b_screen_y, 0);
    printf("line(%d, %d, %d, %d)\n", a_screen_x, a_screen_y, b_screen_x, b_screen_y);
    line(screen, b_screen_x, b_screen_y, c_screen_x, c_screen_y, 0);
    printf("line(%d, %d, %d, %d)\n", b_screen_x, b_screen_y, c_screen_x, c_screen_y);
    line(screen, c_screen_x, c_screen_y, a_screen_x, a_screen_y, 0);
    printf("line(%d, %d, %d, %d)\n", c_screen_x, c_screen_y, a_screen_x, a_screen_y);
  }
}



/* Position an object */

/* rotation: alpha = acos(u.v/(||u||.||v||))
   scrap (||u||.||v||) because their are unit vectors
   World's X unit vector is (1, 0, 0);
   u.v = (1,0,0).(x,y,z) = 1.x + 0.y + 0.z = x
   => alpha = acos(x)
   Same for X and Z.
*/
/*
  - move to origin
  - rotate X with alpha = acos(x)
  - rotate Y with alpha = acos(y)
  - rotate Z with alpha = acos(z)
  - move to camera (cam.center)
*/
